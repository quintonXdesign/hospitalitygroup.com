/* Author: John Gibby @thatgibbyguy */

// ==========================
/* Store current location */
// ==========================

var pathName = location.pathname;

// ==========================
/* Remove Phone Link on Desktop */
// ==========================

remPhoneLink = function(){
	docWidth = $(document).width();
	if (docWidth >= 1024){
		$('[data-query="phone"]').click(function(){
			return false;
		});
	}
	else{
		$('[data-query="phone"]').click(function(){
			return true;
		});
	}
};
remPhoneLink();

// ==========================
/* Adding and removing classes */
// ==========================

var addClassTo = function(query){
	$('[data-query="' + query + '"]').addClass('on');
};
var removeClassFrom = function(query){
	$('[data-query="' + query + '"]').removeClass('on');
};
var addClassByUrl = function(path,query){
	if(pathName.indexOf(path) >= 0){
		$('[data-query="' + query + '"]').addClass('on');
	}
};

// ==========================
/* Mobile Menu Trigger */
// ==========================

var nav = '';
var toggler = document.getElementById('mobile-toggle');

$(toggler).click(function(){
	$(nav).toggleClass('mobile-hidden').toggleClass('show');
});

// ==============================================
/* JVFloat - Placeholder Effect*/
// ==============================================

//apply effect
if(!$('html').hasClass('lt-ie9')){
	$('.float-pattern').jvFloat();
}

// ==============================================
/* Header Slider */
// ==============================================
$("#slider").cycle({
	fx:"fade",
	speed:1050,
	timeout:3500
});

// ==============================================
/* Height Fix */
// ==============================================
var homeFix = function(){
	headerHeight = $('header').outerHeight();
	footerHeight = $('footer').outerHeight();
	headFootHeight = headerHeight + footerHeight;
	windowHeight = $(window).height();
	containerHeight = windowHeight - (headFootHeight);

	$('#content').outerHeight(containerHeight);
};
if($(window).height() > 830) {
	if($('body').hasClass('Home')){
		
		homeFix();

		$(window).resize(function(){
			headerHeight = $('header').height();

			homeFix();
		});

	}
}

// ----------------------------
/* maximage*/
// ----------------------------

$(function(){
	
		$('#slider').maximage({
	        cycleOptions: {
	            speed: 1200,
	            timeout: 6000,
	        },
	        onImagesLoaded: function(){
	        	$('#slider').fadeIn();
	        }
	    });
	
});

